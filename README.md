# finda.fish
[finda.fish](https://finda.fish) is an online poker tracker for [PokerTH](https://pokerth.net).
It is inspired by professional products like _Poker Tracker_ or _Holdem Manager_.
While [its predecessor](https://github.com/Pik-9/PokerTH-Tracker) only analyzes the local log
files, this tracker attempts to collect game logs from all its users and uses the accumulated
data for analysis.

## Deployment
This software needs a MySQL/MariaDB Database to store its game data. Make sure to create one.
Running this application server is as simple as:
```bash
node server.js
```

But you may need to pass a few environment variables:
|  Variable      |                                     Meaning                                               |     Default      |
|----------------|-------------------------------------------------------------------------------------------|------------------|
| NODE\_ENV      | The environment. Note that the app behaves differently in `development` and `production`. | development      |
| DB\_HOST       | The database server.                                                                      | localhost        |
| DB\_USER       | The database user name.                                                                   | fish             |
| DB\_PASS       | The password for the database user.                                                       | \<empty\>        |
| DB\_NAME       | The database name to connect to.                                                          | finda\_fish\_dev |
| DB\_DEBUG      | Debug mode - Database queries get written to stdout.                                      | false            |
| MATOMO\_URL    | The URL of your matomo tracker. (optional)                                                | \<empty\>        |
| MATOMO\_SITEID | The site id of your matomo tracker. (optional)                                            | \<empty\>        |

The `development` environment is unsafe and should only be used for ...well _development_.
Make sure to pass
```bash
export NODE_ENV=production
```
before exposing this app to the internet.

## Player type analysis
Every known player is categorized according to his statistical values.
See this graph:
![Player type categorization](./misc/poker-verdict.png)

## License
finda.fish is free software. You can redistribute it under the terms of the GNU Affero General
Public License in version 3 or at your choice any later version.
See: [AGPLv3](./LICENSE)

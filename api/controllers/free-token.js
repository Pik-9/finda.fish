/**
 * @file Get a free token for development.
 * @author Daniel Steinhauer
 */

import { generateToken } from '../helpers/token.js';
import RequestError from '../helpers/request-error.js';

/**
 * Generate a free token in development mode.
 *
 * @throws RequestError if in production mode.
 * @return A new token.
 */
export default async function freeToken() {
  // In production we cannot be that generous.
  if (process.env.NODE_ENV === 'production') {
    throw new RequestError('Forbidden', 403);
  }

  return generateToken();
}

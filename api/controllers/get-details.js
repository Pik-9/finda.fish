/**
 * @file Get detailed player values with their PokerTH metainfo.
 * @author Daniel Steinhauer
 */

import { enrichPlayerObject } from '../helpers/pokerth-metainfo.js';
import { getPlayerStats } from '../helpers/analyze.js';

/**
 * Get player stats including PokerTH metadata.
 *
 * @return {Object} Enhanced player stats.
 */
export default async function getDetailedPlayerInfo() {
  const playerStats = await getPlayerStats();
  const enrichedPlayerStats = await enrichPlayerObject(playerStats);

  return enrichedPlayerStats;
}

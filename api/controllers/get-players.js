/**
 * @file Retrieve a short list of playernames and their observed hand counts.
 * @author Daniel Steinhauer
 */

import { getPlayerStats } from '../helpers/analyze.js';

/**
 * Get a list of all players with only their names and observed hand counts.
 *
 * @return {Array} List of objects {name: {String}, observedHands: {Number}}.
 */
export default async function getAllPlayers() {
  const allStats = await getPlayerStats();
  return Object.entries(allStats).map(([playerName, playerStats]) => ({
    name: playerName,
    observedHands: playerStats.observedHands,
  }));
}

/**
 * @file Get a player's full profile information.
 * @author Daniel Steinhauer
 */

import RequestError from '../helpers/request-error.js';
import { getPokerTHInfo } from '../helpers/pokerth-metainfo.js';
import { getPlayerStats } from '../helpers/analyze.js';

/**
 * Get a player's profile with his stats, name and PokerTH metadata.
 *
 * @param request The express.js request object.
 * @throws RequestError if player does not exist.
 * @return {Object} The player's profile.
 */
export default async function getProfile(request) {
  const [pokerthInfo, playerStats] = await Promise.all([
    getPokerTHInfo(request.params.username),
    getPlayerStats(),
  ]);

  if (!(request.params.username in playerStats)) {
    throw new RequestError('Not Found', 404);
  }

  return {
    ...playerStats[request.params.username],
    name: request.params.username,
    pokerth: pokerthInfo,
  };
}

/**
 * @file Handle log file upload and extract game data.
 * @author Daniel Steinhauer
 */

import { unlinkSync } from 'fs';

import Game from '../models/game.js';
import extractGameData from '../helpers/log-extractor.js';
import threeMonthsAgo from '../helpers/three-months-ago.js';
import { invalidateCache } from '../helpers/analyze.js';
import { generateToken } from '../helpers/token.js';

const env = process.env.NODE_ENV || 'development';

async function mergeGame(game, existingGames) {
  const ret = {
    addedActions: 0,
    knownActions: 0,
  };

  const existingGame = existingGames.find((egame) => egame.compare(game).equal);

  if (existingGame) {
    const actDifference = game.actions.length - existingGame.actions.length;
    if (actDifference > 0) {
      await existingGame.appendActions(game.actions.slice(existingGame.actions.length));

      ret.addedActions += actDifference;
      ret.knownActions += existingGame.actions.length;
    } else {
      ret.knownActions += game.actions.length;
    }
  } else {
    await Game.createGame(game.commenceTime, game.actions);

    ret.addedActions += game.actions.length;
  }

  if (ret.addedActions > 0) {
    invalidateCache();
  }

  return ret;
}

async function extractData(files) {
  let defunctFiles = 0;

  const knownGames = await Game.getAllGames(threeMonthsAgo());
  const extractedGames = files.reduce((accu, file) => {
    let extr;
    try {
      extr = extractGameData(file.path);
    } catch (err) {
      if (env !== 'production') {
        process.stderr.write(`${err.stack}\n`);
      }
      /* eslint-disable-next-line no-plusplus */
      defunctFiles++;
    }

    unlinkSync(file.path);

    return (extr ? accu.concat(extr) : accu);
  }, []);

  const recentGames = extractedGames.filter((game) => game.commenceTime > threeMonthsAgo());

  const results = await Promise.all(recentGames.map((game) => mergeGame(game, knownGames)));

  return results.reduce(
    (accu, result) => ({
      addedActions: accu.addedActions + result.addedActions,
      knownActions: accu.knownActions + result.knownActions,
      oldGames: accu.oldGames,
      defunctFiles: accu.defunctFiles,
    }), {
      addedActions: 0,
      knownActions: 0,
      oldGames: extractedGames.length - recentGames.length,
      defunctFiles,
    },
  );
}

/**
 * Extract game data from uploaded log files and get info about extracted data.
 *
 * @param request The express.js request object (including uploaded log files).
 * @return {Object} Information about new/old/added actions and a token if at least 200
 *                  unique actions have been added.
 */
export default async function uploadGameData(request) {
  const ret = await extractData(request.files);

  return (ret.addedActions > 199
    ? { ...ret, token: generateToken() }
    : ret
  );
}

/**
 * @file Map each possible action string in log files to a number.
 * @author Daniel Steinhauer
 */

const action = [
  'starts as dealer',
  'posts small blind',
  'posts big blind',
  'folds',
  'calls',
  'is all in with',
  'shows',
  'has',
  'wins',
  'sits out',
  'has left the game',
  'checks',
  'bets',
  'wins (side pot)',
  'wins game',
  'is game admin now',
];

const actionNumber = {};
/* eslint-disable-next-line no-plusplus */
for (let ii = 0; ii < action.length; ++ii) {
  actionNumber[action[ii]] = ii;
}

Object.freeze(action);
Object.freeze(actionNumber);

export { action, actionNumber };

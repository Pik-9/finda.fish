/**
 * @file The statistical analysis of game data in the database.
 * @author Daniel Steinhauer
 */

import Game from '../models/game.js';
import threeMonthsAgo from './three-months-ago.js';
import { actionNumber } from './actions-mapping.js';
import getVerdict from './verdict.js';

let precalculatedStats;
let cacheInvalid = true;

function addStats(obj1, obj2) {
  const ret = {};
  const allKeys = Array.from(new Set(
    Object.keys(obj1).concat(Object.keys(obj2)),
  ));

  allKeys.forEach((key) => {
    const firstArg = obj1[key] || 0;
    const secondArg = obj2[key] || 0;
    ret[key] = (typeof (firstArg) === 'number' ? firstArg : 0)
      + (typeof (secondArg) === 'number' ? secondArg : 0);
  });

  return ret;
}

function getHandCounters(hand) {
  const playerCounters = {};
  hand.players.forEach((player) => {
    // When did the player leave the hand?
    const handOutAction = hand.actions.find((act) => act.player === player
      && (
        act.action === actionNumber.folds
        || act.action === actionNumber.has
        || act.action === actionNumber.shows
      ));

    // If the player didn't even participated in this hand, skip.
    if (!handOutAction) {
      return;
    }

    const handOut = handOutAction.bettingRound;

    const countActionInRound = (actionStr, actionList) => actionList.filter(
      (act) => act.player === player && act.action === actionNumber[actionStr],
    ).length;

    // If the player shoved all in, note the betting round.
    // If not, choose -1.
    const allInAction = hand.actions.find(
      (act) => act.player === player && act.action === actionNumber['is all in with'],
    );
    const allInRound = (allInAction ? allInAction.bettingRound : -1);

    const ret = {
      observedHands: 1,
      seenFlop: (handOut > 0 ? 1 : 0),
      seenTurn: (handOut > 1 ? 1 : 0),
      seenRiver: (handOut > 2 ? 1 : 0),
      wentToShowdown: (handOut > 3 ? 1 : 0),

      winnings: hand.rounds.showdown.filter(
        (act) => act.player === player
          && [actionNumber.wins, actionNumber['wins (side pot)']].includes(act.action),
      ).length,

      countPreflopChecks: countActionInRound('checks', hand.rounds.preflop),
      countFlopChecks: countActionInRound('checks', hand.rounds.flop),
      countTurnChecks: countActionInRound('checks', hand.rounds.turn),
      countRiverChecks: countActionInRound('checks', hand.rounds.river),

      countPreflopCalls: countActionInRound('calls', hand.rounds.preflop),
      countFlopCalls: countActionInRound('calls', hand.rounds.flop),
      countTurnCalls: countActionInRound('calls', hand.rounds.turn),
      countRiverCalls: countActionInRound('calls', hand.rounds.river),

      countPreflopFolds: countActionInRound('folds', hand.rounds.preflop),
      countFlopFolds: countActionInRound('folds', hand.rounds.flop),
      countTurnFolds: countActionInRound('folds', hand.rounds.turn),
      countRiverFolds: countActionInRound('folds', hand.rounds.river),

      countPreflopBets: countActionInRound('bets', hand.rounds.preflop)
        + (allInRound === 0 ? 1 : 0),
      countFlopBets: countActionInRound('bets', hand.rounds.flop)
        + (allInRound === 1 ? 1 : 0),
      countTurnBets: countActionInRound('bets', hand.rounds.turn)
        + (allInRound === 2 ? 1 : 0),
      countRiverBets: countActionInRound('bets', hand.rounds.river)
        + (allInRound === 3 ? 1 : 0),

      shovedAllIn: (allInRound > -1 ? 1 : 0),
    };

    ret.vpip = (ret.countPreflopCalls + ret.countPreflopBets > 0 ? 1 : 0);
    ret.preflopRaise = (ret.countPreflopBets > 0 ? 1 : 0);
    ret.wwsf = (ret.winnings > 0 && ret.seenFlop > 0 ? 1 : 0);
    ret.wsd = (ret.winnings > 0 && ret.wentToShowdown > 0 ? 1 : 0);
    ret.contiBet = (ret.countFlopBets > 0 && ret.preflopRaise === 1 ? 1 : 0);
    ret.chanceToContiBet = (ret.preflopRaise === 1 && ret.seenFlop === 1 && allInRound !== 0
      ? 1
      : 0
    );
    ret.betRiver = (ret.countRiverBets > 0 ? 1 : 0);

    playerCounters[player] = ret;
  });

  return playerCounters;
}

function sumUpHands(hands) {
  const ret = {};

  hands.forEach((hand) => {
    Object.entries(getHandCounters(hand)).forEach(([playerName, playerStat]) => {
      const existingStat = ret[playerName] || {};
      ret[playerName] = addStats(playerStat, existingStat);
    });
  });

  return ret;
}

function sumUpPlayerStats(allHands) {
  const fullringHands = allHands.filter((hand) => hand.countPlayers > 6);
  const shorthandHands = allHands.filter((hand) => hand.countPlayers < 7 && hand.countPlayers > 2);
  const headsupHands = allHands.filter((hand) => hand.countPlayers === 2);

  const fullringStats = sumUpHands(fullringHands);
  const shorthandStats = sumUpHands(shorthandHands);
  const headsupStats = sumUpHands(headsupHands);

  const allKnownPlayers = Array.from(new Set(
    Object.keys(fullringStats).concat(
      Object.keys(shorthandStats),
      Object.keys(headsupStats),
    ),
  ));

  const ret = {};
  allKnownPlayers.forEach((playerName) => {
    ret[playerName] = {
      fullring: fullringStats[playerName] || { observedHands: 0 },
      shorthand: shorthandStats[playerName] || { observedHands: 0 },
      headsup: headsupStats[playerName] || { observedHands: 0 },
    };
  });

  return ret;
}

function safeDivide(num, denom) {
  const dm = (typeof (denom) === 'number' ? denom : 0);
  const nm = (typeof (num) === 'number' ? num : 0);
  return (dm !== 0 ? nm / dm : 0);
}

function calculateValues(stat) {
  return {
    observedHands: stat.observedHands,
    flopSeen: safeDivide(stat.seenFlop, stat.observedHands),
    turnSeen: safeDivide(stat.seenTurn, stat.observedHands),
    riverSeen: safeDivide(stat.seenRiver, stat.observedHands),
    wentToShowdown: safeDivide(stat.wentToShowdown, stat.seenFlop),
    flopAggression: safeDivide(stat.countFlopBets, stat.countFlopChecks + stat.countFlopCalls),
    turnAggression: safeDivide(stat.countTurnBets, stat.countTurnChecks + stat.countTurnCalls),
    riverAggression: safeDivide(stat.countRiverBets, stat.countRiverChecks + stat.countRiverCalls),
    aggression: safeDivide(
      stat.countFlopBets + stat.countTurnBets + stat.countRiverBets,
      stat.countFlopChecks + stat.countTurnChecks + stat.countRiverChecks
        + stat.countFlopCalls + stat.countTurnCalls + stat.countRiverCalls,
    ),
    vpip: safeDivide(stat.vpip, stat.observedHands),
    preflopRaise: safeDivide(stat.preflopRaise, stat.observedHands),
    contiBet: safeDivide(stat.contiBet, stat.chanceToContiBet),
    wonFlop: safeDivide(stat.wwsf, stat.seenFlop),
    wonShowdown: safeDivide(stat.wsd, stat.wentToShowdown),
    betRiver: safeDivide(stat.betRiver, stat.seenRiver),
  };
}

async function loadAllHands() {
  const games = await Game.getAllGames(threeMonthsAgo());
  return games.reduce((accu, value) => accu.concat(value.getActionsByHand()), []);
}

/**
 * Force a recalculation for the next request.
 */
export function invalidateCache() {
  cacheInvalid = true;
}

/**
 * Get all known players of the last 3 months and their stats.
 *
 * @return {Object} All players' statistical values with their player name as keys.
 */
export async function getPlayerStats() {
  if (cacheInvalid || !precalculatedStats) {
    const playerSums = sumUpPlayerStats(await loadAllHands());
    const ret = {};
    Object.entries(playerSums).forEach(([playerName, playerStats]) => {
      ret[playerName] = {
        observedHands: playerStats.fullring.observedHands
          + playerStats.shorthand.observedHands
          + playerStats.headsup.observedHands,

        fullring: calculateValues(playerStats.fullring),
        shorthand: calculateValues(playerStats.shorthand),
        headsup: calculateValues(playerStats.headsup),
      };

      ret[playerName].verdict = getVerdict(ret[playerName]);
    });
    precalculatedStats = ret;
    cacheInvalid = false;
  }

  return precalculatedStats;
}

/**
 * @file Manage database connection and fire requests.
 * @author Daniel Steinhauer
 */

import { createPool } from 'mysql';

let pool;

function asyncQuery(query) {
  return new Promise((resolve, reject) => {
    pool.query(query, (err, response, fields) => {
      if (err) {
        reject(err);
      }

      resolve(response, fields);
    });
  });
}

async function preparePool() {
  pool = createPool({
    connectionLimit: process.env.DB_RATELIMIT || 100,
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'fish',
    password: process.env.DB_PASS || '',
    database: process.env.DB_NAME || 'finda_fish_dev',
    debug: typeof process.env.DB_DEBUG !== 'undefined',
  });

  await asyncQuery(`
    CREATE TABLE IF NOT EXISTS games (
      id INT AUTO_INCREMENT PRIMARY KEY, commence_time DATE
    )
  `);

  await asyncQuery(`
    CREATE TABLE IF NOT EXISTS actions (
      id INT AUTO_INCREMENT PRIMARY KEY,
      game_id INT,
      player VARCHAR(128),
      hand SMALLINT,
      betting_round TINYINT,
      action TINYINT,
      amount MEDIUMINT,
      FOREIGN KEY (game_id) REFERENCES games(id) ON DELETE CASCADE
    )
  `);
}

/**
 * Execute a SQL query.
 *
 * @param {string} query The query to execute.
 * @return The query result.
 */
export default async function execQuery(query) {
  if (!pool) {
    await preparePool();
  }

  return asyncQuery(query);
}

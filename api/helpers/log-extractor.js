/**
 * @file Extract game data from sqlite3 log files.
 * @author Daniel Steinhauer
 */

import Database from 'better-sqlite3';

import { actionNumber } from './actions-mapping.js';

/**
 * Extract game data from one given sqlite3 database.
 *
 * Note that this function does not include anything into the database.
 * It just extracts data objects from .pdb files.
 *
 * @param {string} filepath The path to the .pdb file (sqlite3 database).
 * @return {Array} List of all extracted game objects including their actions.
 */
export default function extractGameData(filepath) {
  const db = new Database(filepath, {
    readonly: true,
    fileMustExist: true,
  });

  const getDateTimeStmt = db.prepare('SELECT Date, Time FROM Session LIMIT 1');
  const sessionDateTime = getDateTimeStmt.get();

  const commenceTime = new Date(`${sessionDateTime.Date} ${sessionDateTime.Time}`);

  const getGamesStmt = db.prepare(
    'SELECT Game.UniqueGameID as id FROM Game WHERE Game.StartSb = 50 AND Game.Startmoney = 10000',
  );

  const validGames = getGamesStmt.all();

  const getActionsStmt = db.prepare(`
    SELECT
      Game.UniqueGameID AS gameId,
      Player.Player,
      Action.HandID,
      Action.BeRo,
      Action.Action,
      Action.Amount
    FROM Game
    INNER JOIN Action USING(UniqueGameID)
    INNER JOIN Player ON Action.Player = Player.Seat
    AND Player.UniqueGameID = Game.UniqueGameID
    ORDER BY Game.UniqueGameID, Action.ActionID
  `);

  const allActions = getActionsStmt.all();

  const ret = validGames.map((game) => ({
    commenceTime,
    actions: allActions.filter((act) => act.gameId === game.id).map((act) => ({
      player: act.Player,
      hand: act.HandID,
      bettingRound: act.BeRo,
      action: actionNumber[act.Action],
      amount: act.Amount,
    })),
  }));

  db.close();

  return ret;
}

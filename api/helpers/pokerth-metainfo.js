/**
 * @file Fetch player metadata from https://pokerth.net.
 * @author Daniel Steinhauer
 */

import axios from 'axios';

let lastFetch;
let cachedRanking;

async function fetchRanking() {
  const data = await axios.request({
    method: 'post',
    url: 'https://pokerth.net/pthranking/ranking/leaderboard',
    headers: {
      'content-type': 'application/json',
      charset: 'UTF-8',
      'sec-fetch-site': 'cross-site',
      'sec-fetch-mode': 'cors',
    },
    data: {
      type: 'init',
      page: 1,
      pageSize: 2000,
      sort: {
        prop: 'final_score',
        order: 'descending',
      },
      filters: {
      },
    },
  });

  return data.data.data;
}

async function getRanking() {
  const now = new Date();
  if (!cachedRanking) {
    cachedRanking = await fetchRanking();
    lastFetch = now;
  }

  if (now - lastFetch > 1000 * 60 * 60) {
    cachedRanking = await fetchRanking();
    // We don't use now here, because some seconds might have passed.
    lastFetch = new Date();
  }

  return cachedRanking;
}

/**
 * Enrich an Object containing all players and their stats with PokerTH metadata.
 *
 * @param {Object} players The object containging player stats with their names as keys.
 * @return {Object} The players object with an additional key 'pokerth' for each player.
 */
export async function enrichPlayerObject(players) {
  const ranking = await getRanking();
  const ret = {};
  Object.entries(players).forEach(([playerName, playerStats]) => {
    ret[playerName] = {
      ...playerStats,
      pokerth: ranking.find((pth) => pth.username === playerName),
    };
  });

  return ret;
}

/**
 * Get the PokerTH metadata for a specific player including his avatar.
 *
 * @param {String} playername The player's name.
 * @return {Object} The player's PokerTH rank and avatar.
 *
 * @todo Fetch the real avatar instead of dummy avatar. Currently not supported on PokerTH's side.
 */
export async function getPokerTHInfo(playername) {
  const ranking = await getRanking();
  const ret = ranking.find((pth) => pth.username === playername);

  if (ret) {
    ret.avatar = '/pokerth.svg';
  }

  return ret;
}

/**
 * @file An exception that is thrown inside request handlers (controllers).
 * @author Daniel Steinhauer
 */

/**
 * @class RequestError represents any kind of unusal (!= 200) termination of a request.
 */
export default class RequestError extends Error {
  constructor(message = 'Bad request', statusCode = 400) {
    super(message);
    this.statusCode = statusCode;
  }
}

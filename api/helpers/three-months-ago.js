/**
 * @file A function that returns a date three months in the past.
 * @author Daniel Steinhauer
 */

/**
 * Get the first of the month three months ago.
 *
 * @return {Date} The date three months ago.
 */
export default function threeMonthsAgo() {
  const today = new Date();
  return new Date(Date.UTC(
    today.getMonth() < 3 ? today.getFullYear() - 1 : today.getFullYear(),
    (today.getMonth() + 9) % 12,
    1,
  ));
}

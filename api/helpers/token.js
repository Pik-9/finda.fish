/**
 * @file All the token handling routines.
 * @author Daniel Steinhauer
 */

import { generateKeyPairSync, createPublicKey, createPrivateKey } from 'crypto';
import { readFileSync, writeFileSync } from 'fs';

import expressJwt from 'express-jwt';
import jsonwebtoken from 'jsonwebtoken';

const pubKeyPath = './public.pem';
const secKeyPath = './private.pem';

let encryptionKey;
let jwt;

function getEncryptionKey() {
  if (!encryptionKey) {
    try {
      const pubPem = readFileSync(pubKeyPath);
      const secPem = readFileSync(secKeyPath);
      encryptionKey = {
        publicKey: createPublicKey(pubPem),
        privateKey: createPrivateKey(secPem),
        publicKeyPEM: pubPem,
        privateKeyPEM: secPem,
      };
    } catch (err) {
      encryptionKey = generateKeyPairSync('ec', {
        namedCurve: 'secp521r1',
      });

      encryptionKey.publicKeyPEM = encryptionKey.publicKey.export(
        {
          type: 'spki',
          format: 'pem',
        },
      );

      encryptionKey.privateKeyPEM = encryptionKey.privateKey.export(
        {
          type: 'pkcs8',
          format: 'pem',
        },
      );

      writeFileSync(pubKeyPath, encryptionKey.publicKeyPEM);
      writeFileSync(secKeyPath, encryptionKey.privateKeyPEM);
    }
  }

  return encryptionKey;
}

/**
 * Get the single instance of JWT express middleware.
 *
 * @return The expressJWT instance.
 */
export function getExpressJwt() {
  if (!jwt) {
    jwt = expressJwt({
      secret: getEncryptionKey().publicKeyPEM,
      algorithms: ['ES512'],
    });
  }

  return jwt;
}

/* eslint-disable no-unused-vars */

/**
 * An express handler that sends an error response if something went wrong.
 *
 * This is to prevent express from failing with an exception.
 */
export function middleErrorHandler(error, request, response, next) {
  if (error) {
    response.status(401).send('Unauthorized');
  }
}

/* eslint-enable no-unused-vars */

/**
 * Generate a token for three days.
 *
 * @return A valid token.
 */
export function generateToken() {
  const key = getEncryptionKey();
  return jsonwebtoken.sign(
    {},
    key.privateKey,
    {
      algorithm: 'ES512',
      expiresIn: '3 days',
    },
  );
}

/**
 * @file Determine the player type based on his values. The whole decision tree
 * can be seen in `misc/poker-verdict.png`.
 *
 * @author Daniel Steinhauer
 */

function goodDumbLAG(playerData) {
  return (playerData.shorthand.wonFlop > 0.45 ? 'lag' : 'dumbLag');
}

function donkeyFish(playerData) {
  return (playerData.shorthand.wentToShowdown > 0.45 ? 'donkey' : 'fish');
}

function weakDonkey(playerData) {
  return (playerData.shorthand.wonShowdown < 0.4 ? 'donkey' : 'weakPassive');
}

function looseAF(playerData) {
  let ret = 'maniac';

  if (playerData.shorthand.aggression < 2.0) {
    ret = donkeyFish(playerData);
  } else if (playerData.shorthand.aggression <= 4.0) {
    ret = goodDumbLAG(playerData);
  }

  return ret;
}

function tightAF(playerData) {
  return (playerData.shorthand.aggression < 1.5
    ? weakDonkey(playerData)
    : 'shark'
  );
}

function highVPIP(playerData) {
  return (playerData.shorthand.vpip - playerData.shorthand.preflopRaise < 0.1
    ? looseAF(playerData)
    : donkeyFish(playerData)
  );
}

function middleVPIP(playerData) {
  return (playerData.shorthand.vpip - playerData.shorthand.preflopRaise > 0.07
    ? donkeyFish(playerData)
    : tightAF(playerData)
  );
}

function lowVPIP(playerData) {
  return (playerData.shorthand.vpip - playerData.shorthand.preflopRaise > 0.05
    ? 'rock'
    : 'weakPassive'
  );
}

function analyzeVPIP(playerData) {
  let ret;

  if (playerData.shorthand.vpip < 0.15) {
    ret = lowVPIP(playerData);
  } else if (playerData.shorthand.vpip > 0.25) {
    ret = highVPIP(playerData);
  } else {
    ret = middleVPIP(playerData);
  }

  return ret;
}

function isAllInTroll(playerData) {
  // TODO: If all-in > 15%
  return analyzeVPIP(playerData);
}

/**
 * Return the verdict based on the statistical values.
 *
 * @param {Object} playerData The player's values.
 * @return {string} The player type.
 */
export default function getVerdict(playerData) {
  return (playerData.observedHands < 150
    ? 'undetermined'
    : isAllInTroll(playerData)
  );
}

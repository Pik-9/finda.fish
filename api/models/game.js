/**
 * @file The basic model `Game` of this app. It's the interface to the database.
 * @author Daniel Steinhauer
 */

import { format } from 'mysql';

import execQuery from '../helpers/db-query.js';
import { actionNumber } from '../helpers/actions-mapping.js';

/**
 * @class Game representing the game of poker with all its actions.
 */
export default class Game {
  /**
   * Create a new Game. This is not meant to be called directly. Use the static methods instead.
   *
   * @param {number} id The id in the database.
   * @param {Date} commenceTime the time this game took place.
   * @param {Array} actions The game actions.
   */
  constructor(id, commenceTime, actions) {
    this.id = id;
    this.commenceTime = commenceTime;
    this.actions = actions;
  }

  /**
   * Append actions to this game and insert them into the database.
   *
   * @param {Array} actions The actions to insert.
   */
  async appendActions(actions) {
    const actionsArray = actions.map(
      (action) => [
        this.id,
        action.player,
        action.hand,
        action.bettingRound,
        action.action,
        action.amount,
      ],
    );

    const actionsQry = format(
      'INSERT INTO actions (game_id, player, hand, betting_round, action, amount) VALUES ?',
      [actionsArray],
    );

    const actionsResult = await execQuery(actionsQry);

    const newActions = actions.map((action, index) => ({
      id: actionsResult.insertId + index,
      gameId: this.id,
      ...action,
    }));

    this.actions = this.actions.concat(newActions);
  }

  /**
   * Compare to games by their actions since the commenceTime may differ.
   *
   * @param {Game} otherGame The other game instance.
   * @return {Object} {equal: {boolean}, difference: {number}}.
   */
  compare(otherGame) {
    const cmp = (act1, act2) => act1.player === act2.player
      && act1.hand === act2.hand
      && act1.bettingRound === act2.bettingRound
      && act1.action === act2.action
      && act1.amount === act2.amount;

    const minLen = (this.actions.length > otherGame.actions.length
      ? this.actions.length
      : otherGame.actions.length);

    let equal = true;
    /* eslint-disable-next-line no-plusplus */
    for (let ii = 0; ii < minLen; ++ii) {
      if (!cmp(this.actions[ii], otherGame.actions[ii])) {
        equal = false;
        break;
      }
    }

    return {
      equal,
      difference: otherGame.actions.length - this.actions.length,
    };
  }

  /**
   * Order all actions of this game by the hand played.
   *
   * @return {Array} A list of objects representing each played hand.
   */
  getActionsByHand() {
    const allHands = Array.from(new Set(this.actions.map((act) => act.hand)));
    return allHands.map((handId) => {
      const handActions = this.actions.filter(
        (act) => act.hand === handId
          && act.action !== actionNumber['has left the game']
          && act.action !== actionNumber['sits out'],
      );
      const rounds = {
        preflop: handActions.filter((act) => act.bettingRound === 0),
        flop: handActions.filter((act) => act.bettingRound === 1),
        turn: handActions.filter((act) => act.bettingRound === 2),
        river: handActions.filter((act) => act.bettingRound === 3),
        showdown: handActions.filter((act) => act.bettingRound === 4),
      };

      const allPlayers = new Set(rounds.preflop.map((act) => act.player));
      const bigBlind = rounds.preflop.find((act) => act.action === actionNumber['posts big blind']).amount;

      return {
        actions: handActions,
        rounds,
        hand: handId,
        countPlayers: allPlayers.size,
        players: Array.from(allPlayers),
        bigBlind,
      };
    });
  }

  /**
   * Get a list of all games that occured after 'since'.
   *
   * @param {Date} since Do not include games before this data.
   * @return {Array} List of games.
   */
  static async getAllGames(since) {
    const timelyFilter = (since
      ? `WHERE games.commence_time > '${since.toISOString().slice(0, 10)}'`
      : ''
    );

    const qry = `
      SELECT
        games.id as 'gameId',
        games.commence_time as 'commenceTime',
        actions.id as 'actionId',
        actions.player,
        actions.hand,
        actions.betting_round as 'bettingRound',
        actions.action,
        actions.amount 
      FROM games INNER JOIN actions
      ON games.id = actions.game_id
      ${timelyFilter}
      ORDER BY gameId, actions.id
    `;

    const result = await execQuery(qry);

    const ret = [];
    let lastGameId = -1;
    result.forEach((row) => {
      let game;
      if (row.gameId !== lastGameId) {
        game = new Game(row.gameId, row.commenceTime, []);
        ret.push(game);
      } else {
        game = ret[ret.length - 1];
      }

      lastGameId = row.gameId;

      game.actions.push({
        id: row.actionId,
        player: row.player,
        hand: row.hand,
        bettingRound: row.bettingRound,
        action: row.action,
        amount: row.amount,
      });
    });

    return ret;
  }

  /**
   * Insert a new game into the database.
   *
   * @param {Date} commenceTime The time the game occured.
   * @param {Array} hands The hand actions of this game.
   *
   * @return {Game} The newly created game instance.
   */
  static async createGame(commenceTime, hands) {
    const commenceDate = commenceTime.toISOString().slice(0, 10);
    const gameResult = await execQuery(
      `INSERT INTO games (commence_time) VALUES ('${commenceDate}')`,
    );

    const ret = new Game(gameResult.insertId, commenceTime, []);
    await ret.appendActions(hands);
    return ret;
  }
}

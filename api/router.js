/**
 * @file The router for the backend API. All API calls go through here.
 * @author Daniel Steinhauer
 */

import { Router } from 'express';
import multer from 'multer';

import { getExpressJwt, middleErrorHandler } from './helpers/token.js';
import RequestError from './helpers/request-error.js';

import getAllPlayers from './controllers/get-players.js';
import getDetailedPlayerInfo from './controllers/get-details.js';
import getProfile from './controllers/get-profile.js';
import uploadGameData from './controllers/upload.js';
import freeToken from './controllers/free-token.js';

const upload = multer({
  dest: 'tmp/',
  limits: 1048576,
});

function routeWrapper(handler) {
  return (request, response) => {
    handler(request).then((data) => {
      response.type('json').send(data);
    }).catch((error) => {
      const erMsg = error.message || 'Bad Request';
      const erCode = error.statusCode || 400;
      if (!(error instanceof RequestError)) {
        process.stderr.write(`${error.stack}\n`);
      }
      response.status(erCode).send(erMsg);
    });
  };
}

/**
 * Create a router for all backend API calls.
 *
 * @return The backend router.
 */
export default function createApiRouter() {
  const router = Router();

  // Give away a free token; disabled in production.
  router.get('/token', routeWrapper(freeToken));

  router.get('/all', routeWrapper(getAllPlayers));

  router.post('/gamedata',
    upload.array('dbfiles', 100),
    routeWrapper(uploadGameData));

  router.get('/player/:username',
    getExpressJwt(),
    middleErrorHandler,
    routeWrapper(getProfile));

  router.get('/table',
    getExpressJwt(),
    middleErrorHandler,
    routeWrapper(getDetailedPlayerInfo));

  return router;
}

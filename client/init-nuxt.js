import nuxt from 'nuxt';

const isDev = process.env.NODE_ENV !== 'production';

export default async function createNuxt() {
  const instance = await nuxt.loadNuxt(isDev ? 'dev' : 'start');
  if (isDev) {
    await nuxt.build(instance);
  }
  return instance;
}

export default {
  // Landing page
  'title-finda-fish': 'finda.fish',
  'title-tracking-what': 'Was tut ein Poker Tracker?',
  'title-online-tracking': 'Online Tracker',
  'title-how-works': 'So funktioniert es',
  'title-log-donations': 'Datenspende durch Logdateien',
  'title-what-get': 'Und dann...',
  'title-enjoy': 'Viel Spaß',

  'text-welcome': `
    Willkommen bei finda.fish - einen online Poker Tracker für
    <a href="https://pokerth.net">PokerTH</a>. Hier wird die Spielweise
    der bei PokerTH aktiven Spieler analysiert auf Grundlage der
    zusammengetragenen Logdaten der Spieler.
  `,
  'text-tracker': `
    Diese Software ist professionellen Produkten, wie
    <a href="
    http://www.bestofrakeback.com/poker-articles/poker-help-tips/reading-interpreting-poker-statistics
    ">Poker Tracker oder dem Holdem Manager</a> nachempfunden.
    Diese Software berechnet statistische Werte, wie z.B. <i>VP$IP</i> oder
    den <i>Aggressionsfaktor</i>, anhand derer sich ein Spieler und seine
    Spielweise einschätzen lassen.
  `,
  'text-online': `
    finda.fish ist ein Nachfolger des
    <a href="https://github.com/Pik-9/PokerTH-Tracker">PokerTH Trackers</a>.
    Im Gegensatz zu seinem Vorgänger analysiert <b>finda.fish</b> nicht nur
    die eigenen Logdateien, sondern sammelt die Daten von all seinen Nutzern
    und kann so ein wesentlich größeres Bild vermitteln.
  `,
  'text-how-works': 'Fang gleich an und verschaffe dir einen Überblick über alle bekannten Spieler.',
  'text-log-donations': `
    Um genauere Details über einen bestimmten Spieler zu erhalten, musst
    du allerdings erst eine Datenspende verrichten.
    Dieses Projekt lebt davon, dass viele Spieler ihre Daten hochladen.
  `,
  'text-what-get': `
    Nach dem erfolgreichen Hochladen von <i>neuen</i> Spieldaten kriegst du
    uneingeschränkten Zugriff für drei Tage.
  `,
  'text-enjoy': `
    Viel Spaß beim Pokern. &#128523;
    Hoffentlich kannst du mit den hier gezeigten Information den ein oder
    anderen Gegner bezwingen.
  `,

  'button-get-me-there': 'Auf geht\'s',
  'button-upload-files': 'Hochladen',
  'button-get-started': 'Los geht\'s',

  // Overview page
  'obs-hands': '{0} observierte Hände',
  'table-pth-rank': 'PokerTH Platzierung',
  'table-username': 'Spieler',
  'table-obs-hands': 'Beobachtete Hände',
  'table-verdict': 'Spielertyp',
  'table-filter-hands': 'Nur >150 anzeigen',

  // Profile page
  'profile-pagetitle': 'Analyse von {0}',

  // Tablesize choosing component
  'button-fullring': 'Fullring (7 - 10 Spieler)',
  'button-shorthand': 'Shorthand (3 - 6 Spieler)',
  'button-headsup': 'Heads-Up (2 Spieler)',

  // Get Access Tokens pages
  'free-pagetitle': 'Freies Zugangstoken',
  'submit-pagetitle': 'Lade Deine Logdateien Hoch',
  'submit-files-step': 'Logdateien hochladen',
  'submission-result-step': 'Zusammenfassung',
  'too-few-data-for-login': 'Der Datensatz enthält zu wenig neue Spieleraktionen.',
  'login-after-submission': 'Du hast jetzt Zugriff.',
  'beg-for-data': `
    Bevor es weitergehen kann, musst du uns deine Spieldaten aus PokerTH
    überlassen. Lade einfach deine Logdateien des PokerTH Programmes hoch.
    Siehe weiter unten wie:
  `,
  'instructions-linux': `
    Der Linux Klient speichert seine Logdateien normalerweise in deinem
    Heimverzeichnis unter <code>~/.pokerth/log-files/</code>.
  `,
  'instructions-win': `
    Der Windows Klient speichert seine Logdateien normalerweise in deinem
    Heimverzeichnis unter <code>AppData\\Roaming\\pokerth\\log-files</code>.
  `,
  'instructions-mac': `
    Der MacOS Klient speichert seine Logdateien normalerweise in deinem
    Heimverzeichnis unter <code>~/.pokerth/log-files/</code>.
  `,
  'added-actions-cap': 'Neue Aktionen:',
  'known-actions-cap': 'Bereits bekannte Aktionen:',
  'old-games-cap': 'Spiele älter als drei Monate:',
  'defunct-files-cap': 'Übersprungene Dateien:',
  'free-token-suc': 'Du hast jetzt Zugang.',
  'free-token-error': 'Das hat nicht funktioniert.',
  'get-free-token': 'Freien Zugang erhalten',
  'pokerth-logfiles': 'PokerTH Logdateien (*.pdb)',
  'submit-files': 'Hochladen',

  // Layout
  'nav-overview': 'Spielerübersicht',
  'nav-player': 'Spieler',
  'foot-fork': 'Forken',

  // Analysis cards
  'aggression-title': 'Aggressionsfaktor (AF)',
  'betting-title': 'Setzverhalten',
  'invest-rev-title': 'Investitionsverhalten & Profit',
  'round-title': 'Runden',
  'fold-title': 'Ausstiegsverhalten',

  // Analysis value descriptions
  'desc-wts': `
    Der Wert "Went To Showdown" (WTS) gibt an, wie oft ein Spieler mit
    seinen Karten in den Showdown gegangen ist, nachdem er den Flop
    gesehen hat.
  `,
  'desc-af': `
    Der Aggressinsfaktor (AF) gibt das Verhältnis an zwischen aggressiven
    Zügen (Setzen, Erhöhen, All-In) und passiven Zügen (Schieben, Mitgehen).

    Der AF kann für das gesamte Post-Flop Spiel berechnet werden, oder nur
    für bestimmte Runden (Flop AF, Turn AF, River AF).

    Normalerweise sollte der AF zwischen 1,5 und 2,5 liegen.
    Ein AF < 1,5 deutet häufig auf einen sehr passiven Spieler (Calling Station) hin.
    Starke (mäßig aggressive) Spieler haben häufig einen AF zwischen 2,5 und 3,5.
    Bei einem AF > 3,5 kann man von einem Maniac ausgehen, der immer und alles erhöhrt.
  `,
  'desc-preflop-raise': `
    Wie oft der Spieler vor dem Flop erhöht hat.
  `,
  'desc-continuation-bet': `
    Wie oft der Spieler auf dem Flop noch einmal gesetzt hat, nachdem
    er vor dem Flop schon erhöht hatte.
  `,
  'desc-bet-river': `
    Wie oft der Spieler auf dem River erhöht/setzt.
  `,
  'desc-check-raise': `
    Bei einem Check-Raise schiebt ein Spieler zunächst, nur um einem
    Einsatz seines Gegners dann eine Erhöhung entgegenzusetzen.
    Dies gilt allgemein als sehr starker Spielzug. Wenn du einem Spieler
    begegnest, der dies häufig tut, solltest du vorsichtig sein.
  `,
  'desc-vpip': `
    "Voluntarily Put money ($) In Pot" (VP$IP) gibt an, wie oft ein Spieler
    vor dem Flop in seine Hand investiert hat - entweder durch Mitgehen oder
    durch eine Erhöhung.
  `,
  'desc-wwsf': `
    "Won money ($) When Saw Flop" (W$WSF) gibt an, in wie vielen der Spiele,
    in denen der Spieler den Flop gesehen hat, am Ende auch etwas gewonnen
    hat.
  `,
  'desc-wsd': `
    "Won money ($) in ShowDown" (W$SD) gibt an, wie viel Prozent der Showdowns
    für diesen Spieler mit einem Gewinn endeten.
  `,

  // Player type descriptions
  'desc-allInTroll': `
    Dieser armseelige kleine Wurm weiß
    mit seinem Leben nichts Besseres anzu-
    fangen, als andere Leute zu trollen! Er
    hält sich für unglaublich witzig dafür,
    dass er anderen Spielern auf die Nerven
    geht, indem er ständig mit Müll All-In
    geht! Es beschert ihm eine ganz besondere
    Befriedigung, wenn er sieht, wie die anderen
    Spieler sich im Chatwindow über ihn
    aufregen, also denk dran:
    DON'T FEED THE TROLL!
    Warte einfach auf Premiumhände und
    call ihn.
    Beachte, dass alle unten stehenden
    Hinweise nur gelten, wenn er ausnahmsweise
    mal vernünftig spielt.
  `,
  'desc-donkey': `
    Diese Spieler hat den Sinn des Spiels nicht ganz
    verstanden! Er spielt viel zu viele Hände und
    callt alle Einsätze, immer an ein Wunder glaubend!
    Egal wie schlecht seine Chancen stehen, er wird
    bis zum bitteren Ende dabei bleiben!
    Es sollte nicht schwer sein, ihn zu bezwingen,
    aber komm bloß nicht auf die dumme Idee,
    ihn zu bluffen - Er wird einfach callen und
    sich dann auch noch bestätigt fühlen!
  `,
  'desc-dumbLag': `
    Dieser Schwachkopf glaubt, beim Pokern ginge
    es nur darum, einen auf dicke Hose zu machen!
    Ein Möchtegern-Cowboy der seine Karten
    regelmäßg gnadenlos überspielt, nur um nicht
    für ein Weichei gehalten zu werden! Ich bin nicht
    sicher, ob er sich der Tatsache bewusst ist, dass er
    mit seinem Spiel auf lange Sicht Verlust macht,
    oder ob es ihm einfach egal ist. Jedenfalls ist das
    Beste was du gegen ihn tun kannst, ihn einfach
    seinen Spaß haben zu lassen und ihm - nach
    Möglichkeit - aus dem Weg zu gehen...
  `,
  'desc-fish': `
    Dieser Spieler spielt viel zu viele Hände,
    allerdings gibt er sie zügig auf, wenn er
    nichts getroffen hat! Auch wenn er manchmal
    Draws hinterher jagd, ist er doch meistens
    leicht zu bezwingen. Versuche herauszufinden,
    ob er etwas getroffen hat - Und wenn nicht:
    Übe Druck aus!
  `,
  'desc-fool': `
    Okay...
    Ich weiß wirklich nicht, was dieser Typ hier
    eigentlich spielt, aber mit Texas Hold'em hat es
    nicht viel zu tun! Versuche ihm einfach aus dem Weg
    zu gehen, es sei denn, du weißt genau was du da tust.
  `,
  'desc-lag': `
    Dieser Typ spielt viele Hände sehr aggressiv.
    Allerdings scheint er sehr erfolgreich damit zu
    sein, du tust ihm also Unrecht, wenn du ihn für
    einen idiotischen Bluffer hältst! Denn obwohl er
    viel blufft, weiß er ganz genau, was er da tut und
    er kann ein sehr furchteinflößender Gegner sein.
    Sei also vorsichtig und lass dich von ihm nicht
    "on-tilt" treiben.
  `,
  'desc-maniac': `
    Okay, dieser Typ hat einen Sprung in der Schüssel!
    Er hat einen hyper-aggressiven Spielstil, der nicht
    mehr profitabel sein kann. Er setzt und erhöht, als
    gäbe es kein Morgen mehr! Natürlich blufft er meistens,
    aber du musst trotzdem unbedingt RUHE BEWAHREN.
    Du willst nicht mit AK gegen diesen Typen All-In gehen
    und am Ende gegen ein Dreierpärchen verlieren!
    Gehe ihm also einfach aus dem Weg und warte,
    bis er sich selbst aus dem Spiel gekegelt hat.
  `,
  'desc-undetermined': 'Nicht genug Daten.',
  'desc-rock': `
    Für diesen Spieler stellt eine Erhöhung eine Sünde dar!
    Er spielt nur Premiumhände.
    Du wirst nicht viel Geld mit diesem Spieler verdienen,
    auf der anderen Seite schadet er dir aber auch nicht!
    Normalerweise kannst du diesen Spieler gut bluffen.
    Wenn er allerdings mal setzt, solltest du aussteigen, es
    sei denn, du hältst die Nuts.
  `,
  'desc-shark': `
    Dieser Spieler weiß, was er tut!
    Er weiß genau, welche Hände ein großes
    Potential haben und wie er sie spielen muss!
    Du solltest dich nicht auf unnütze Duelle mit ihm
    einlassen. Trotzdem könnte der ein oder andere
    Bluff funktionieren - Aber sei bloß vorsichtig!
  `,
  'desc-weakPassive': `
    Dieser Spieler hat solch eine Angst davor,
    durch eine Aggression aufzufallen, dass
    er besser in der Bingo-Halle aufgehoben wäre!
    Er spielt einige Hände und hofft auf einen Treffer
    auf dem Flop - mehr fällt ihm nicht ein.
    Du wirst ihn häufig erfolgreich bluffen können.
  `,
};

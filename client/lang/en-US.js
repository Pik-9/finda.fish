export default {
  // Landing page
  'title-finda-fish': 'finda.fish',
  'title-tracking-what': 'Tracking what?',
  'title-online-tracking': 'Online Tracking',
  'title-how-works': 'How It Works',
  'title-log-donations': 'Log File Donations',
  'title-what-get': 'What you\'ll get',
  'title-enjoy': 'Enjoy',

  'text-welcome': `
    Welcome to finda.fish. This is an online Poker Tracker for the
    <a href="https://pokerth.net">PokerTH</a> community. It collects and
    analyzes data about the online players' behaviour.
  `,
  'text-tracker': `
    The tracking is inspired by professional software like
    <a href="
    http://www.bestofrakeback.com/poker-articles/poker-help-tips/reading-interpreting-poker-statistics
    ">Poker Tracker or Holdem Manager</a>. It then calculates important values like
    <i>VP$IP</i> or the <i>Aggression Factor</i> which let you deduce what kind of player
    you are facing and what moves you can expect from him/her.
  `,
  'text-online': `
    Unlike its <a href="https://github.com/Pik-9/PokerTH-Tracker">predecessor</a>, which
    could only analyze your own log files, <b>finda.fish</b> accumulates the log files
    of all its users in order to provide a bigger picture.
  `,
  'text-how-works': 'You can jump in directly and get an overview of already known players.',
  'text-log-donations': `
    If you want more details on that player however, you first need to upload
    your PokerTH log files. This projects depends on its users' data donations in the end.
  `,
  'text-what-get': `
    After you uploaded enough new game data, you will be granted unlimited access
    to this tracker for three days until you need to do the next upload.
  `,
  'text-enjoy': `
    Enjoy playing poker. &#128523;
    And maybe this helps you making smarter decisions in a future online tournament.
  `,

  'button-get-me-there': ' Get me there',
  'button-upload-files': 'Upload log files',
  'button-get-started': 'Let\'s get started',

  // Overview page
  'obs-hands': '{0} observed hands',
  'table-pth-rank': 'PokerTH Rank',
  'table-username': 'Player',
  'table-obs-hands': 'Observed Hands',
  'table-verdict': 'Player Type',
  'table-filter-hands': 'Only show >150',

  // Profile page
  'profile-pagetitle': 'Analysis of {0}',

  // Tablesize choosing component
  'button-fullring': 'Fullring (7 - 10 players)',
  'button-shorthand': 'Shorthand (3 - 6 players)',
  'button-headsup': 'Heads-Up (2 players)',

  // Get Access Tokens pages
  'free-pagetitle': 'Free Access Token',
  'submit-pagetitle': 'Submit Your Log Files',
  'submit-files-step': 'Provide log files',
  'submission-result-step': 'Review',
  'too-few-data-for-login': 'You provided not enough new player action data.',
  'login-after-submission': 'You have been granted access.',
  'beg-for-data': `
    Before you can proceed you need to provide game data to analyze.
    Just upload your PokerTH log files here.
    See instructions below:
  `,
  'instructions-linux': `
    The linux client usually saves its log files in your home directory
    under <code>~/.pokerth/log-files/</code>.
  `,
  'instructions-win': `
    The windows client usually saves its log files in your home directory
    under <code>AppData\\Roaming\\pokerth\\log-files</code>.
  `,
  'instructions-mac': `
    The macOS client usually saves its log files in your home directory
    under <code>~/.pokerth/log-files/</code>.
  `,
  'added-actions-cap': 'New actions:',
  'known-actions-cap': 'Already known actions:',
  'old-games-cap': 'Games older than three months:',
  'defunct-files-cap': 'Skipped files:',
  'free-token-suc': 'You got a token.',
  'free-token-error': 'That went wrong.',
  'get-free-token': 'Get free access',
  'pokerth-logfiles': 'PokerTH log files (*.pdb)',
  'submit-files': 'Submit',

  // Layout
  'nav-overview': 'Player Overview',
  'nav-player': 'Players',
  'foot-fork': 'Fork me',

  // Analysis cards
  'aggression-title': 'Aggression Factor (AF)',
  'betting-title': 'Betting behavior',
  'invest-rev-title': 'Investment & Revenue',
  'round-title': 'Rounds',
  'fold-title': 'Folding behavior',

  // Analysis value descriptions
  'desc-wts': `
    The "Went To Showdown" (WTS) value states how often the player
    kept his cards until the showdown after seeing the flop.
  `,
  'desc-af': `
    The aggression factor (AF) is the times the player commits
    aggressive acts (bet, raise, all in) divided by the times the
    player commits passive acts (check, call).

    The AF is calculated for the whole post flop game and per street (flop, turn river).

    The AF should usually be between 1.5 and 2.5.
    An AF < 1.5 usually indicates a passive calling station.
    An AF between 2.5 and 3.5 can indicate a strong player.
    An AF > 3.5 usually indicates a maniac who raises no matter what.
  `,
  'desc-preflop-raise': `
    This value states how often the player raised his hand preflop.
  `,
  'desc-continuation-bet': `
    This value states how often the player bet again on the flop after
    having done a preflop raise.
  `,
  'desc-bet-river': `
    How often the player bet on the river.
  `,
  'desc-check-raise': `
    A check raise is a raise after a check and a bet from the opponent.
    This is usually done by strong players. You should be aware if this
    player has a high value here.
  `,
  'desc-vpip': `
    "Voluntarily Put money ($) In Pot" (VP$IP) states how often the player
    invested in his hand preflop either by calling or raising.
    Blinds do not count.
  `,
  'desc-wwsf': `
    "Won money ($) When Saw Flop" (W$WSF) states how often the player was
    eventually successfull after seeing the flop.
  `,
  'desc-wsd': `
    "Won money ($) in ShowDown" (W$SD) states how often the player really
    won money after going into the showdown.
  `,

  // Player type descriptions
  'desc-allInTroll': `
    This pathetic little worm's only purpose in
    life is trolling other people! He thinks he
    is incredibly funny, because he annoys other
    players by pushing all-in with junk all the
    time! He loves to see other players complaining
    about him in the chat window, so remember:
    DON'T FEED THE TROLL!
    Just wait for premium hands to call him.
    Attention: The tips below only apply when
    he is playing seriously.
  `,
  'desc-donkey': `
    This player has no idea of what Poker is about!
    He plays far too many hands and calls down till
    the river hoping for a miracle! No matter how
    bad his hand is, he will play it till the end!
    He should be easy to exploit, but don't bluff him!
  `,
  'desc-dumbLag': `
    This guy is a nut, who thinks Poker would be
    about showing off! A wannabe cowboy who is
    regularily overplaying his cards just to not
    look weak! I wonder if he really doesn't notice
    that he's losing in the long run or whether he
    just doesn't care. However the best you can do
    is just letting him have his fun and get out of
    his way...
  `,
  'desc-fish': `
    This player plays far too many hands preflop,
    but he is a fit-or-fold player! He might be chasing
    draws sometimes, but he will usually throw his
    hand away if he didn't hit anything. Try to figure
    out whether he has something and if not: Bluff him
    out!
  `,
  'desc-fool': `
    Well...
    I don't know, what this guys is actually playing,
    but it has not much to do with Texas Hold'em!
    Just get out of his way unless you know what
    you're doing.
  `,
  'desc-lag': `
    This guy likes to play many hands aggressively.
    But he seems to be pretty successfull with it,
    so don't consider him a stupid bluffer! Even
    though he's bluffing a lot he actually knows
    what he's doing and he can be a really strong
    opponent. Be carefull and don't get on tilt.
  `,
  'desc-maniac': `
    Okay, this guy's just cracked! He is hyper
    aggressive in a way that can't be profitable.
    He bets and raises like there's no tomorrow!
    Of course, he is bluffing a lot, but it's
    important that you KEEP CALM. You don't want
    this guy to make you push all-in with AK and
    lose against a pair of threes! So just get
    out of his way and wait until he eliminates
    himself.
  `,
  'desc-undetermined': 'Not sufficient data.',
  'desc-rock': `
    In this player's opinion raising is a sin!
    He plays only premium hands.
    You won't get much money out of this player,
    but he won't hurt you either!
    You can usually bluff him a lot.
    If he bets, you better fold unless you got the
    nuts.
  `,
  'desc-shark': `
    This player actually knows what he's doing!
    He plays only hands which have big potential
    and he knows how to play'em aggressively!
    You should try to avoid getting yourself into
    a duel with him. Though a good bluff might
    succeed sometimes - But BE CAREFULL!
  `,
  'desc-weakPassive': `
    This player is so scared of taking action
    that he would be better off with Bingo!
    He just picks up some nice hands and hopes
    for a hit on the Flop.
    A bluff will succeed often.
  `,
};

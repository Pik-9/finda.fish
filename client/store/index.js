export default {
  state() {
    return {
      tableSize: 0,
      playerList: [],
    };
  },

  mutations: {
    setTablesize(state, ts) {
      if (typeof (ts) !== 'number') {
        throw new Error('The table size needs to be a number.');
      }

      if (![0, 1, 2].includes(ts)) {
        throw new RangeError('The table size can only be 0, 1 or 2.');
      }

      /* eslint-disable-next-line no-param-reassign */
      state.tableSize = ts;
    },

    setPlayers(state, players) {
      /* eslint-disable-next-line no-param-reassign */
      state.playerList = players.sort((pa, pb) => {
        const na = pa.name.toUpperCase();
        const nb = pb.name.toUpperCase();

        if (na < nb) {
          return -1;
        }

        if (na > nb) {
          return 1;
        }

        return 0;
      });
    },
  },

  getters: {
    tablesizeDescription(state) {
      const desc = [
        'fullring',
        'shorthand',
        'headsup',
      ];

      return desc[state.tableSize];
    },
  },

  actions: {
    updatePlayerList(context) {
      this.$axios.$get('/api/all').then((data) => {
        context.commit('setPlayers', data);
      }).catch((error) => {
        /* eslint-disable-next-line no-console */
        console.error(error);
      });
    },
  },
};

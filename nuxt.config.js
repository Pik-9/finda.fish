import { readFileSync } from 'fs';

import colors from 'vuetify/lib/util/colors.js';

const port = process.env.PORT || 8080;

const pkgInfo = JSON.parse(readFileSync('./package.json'));

const nuxtConfig = {
  srcDir: 'client/',

  head: {
    titleTemplate: 'finda.fish - %s',
    title: 'Poker Tracker',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: `
          finda.fish is an online poker tracker for the PokerTH community.
          Upload your PokerTH log files and get a detailed analysis of
          other player's behavior.
        `,
      },
      {
        hid: 'robots',
        name: 'robots',
        content: 'index, follow',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: 'PokerTH, Poker, Tracker, Analyzer',
      },
      {
        hid: 'author',
        name: 'author',
        content: 'Daniel Steinhauer',
      },
      {
        hid: 'copyright',
        name: 'copyright',
        content: 'Daniel Steinhauer',
      },
      {
        hid: 'og-title',
        property: 'og:title',
        content: 'finda.fish - Poker Tracker',
      },
      {
        hid: 'og-type',
        property: 'og:type',
        content: 'website',
      },
      {
        hid: 'og-desc',
        property: 'og:description',
        content: 'An online poker tracker (aka log file analyzer) for PokerTH.',
      },
      {
        hid: 'og-site_name',
        property: 'og:site_name',
        content: 'finda.fish',
      },
      {
        hid: 'og-url',
        property: 'og:url',
        content: 'https://finda.fish',
      },
      {
        hid: 'og-image',
        property: 'og:image',
        content: 'https://finda.fish/og-logo.png',
      },
      {
        hid: 'og-image-width',
        property: 'og:image:width',
        content: '500',
      },
      {
        hid: 'og-image-height',
        property: 'og:image:height',
        content: '500',
      },
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  modules: [
    'nuxt-i18n',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
  ],

  buildModules: [
    '@nuxtjs/vuetify',
  ],

  publicRuntimeConfig: {
    version: pkgInfo.version,
  },

  axios: {
    baseURL: (process.env.NODE_ENV === 'production'
      ? 'https://finda.fish'
      : `http://localhost:${port}`
    ),
  },

  i18n: {
    locales: [
      {
        name: 'English',
        code: 'en',
        iso: 'en-US',
        file: 'en-US.js',
      },
      {
        name: 'Deutsch',
        code: 'de',
        iso: 'de-DE',
        file: 'de-DE.js',
      },
    ],
    lazy: true,
    strategy: 'no_prefix',
    langDir: 'lang/',
    defaultLocale: 'en',
  },

  auth: {
    redirect: {
      login: '/submit',
    },
    strategies: {
      local: false,

      freeDevToken: {
        _scheme: 'local',
        token: {
          property: false,
        },
        endpoints: {
          user: false,
          logout: true,

          login: {
            url: '/api/token',
            method: 'get',
          },
        },
      },

      submitData: {
        _scheme: 'local',
        endpoints: {
          user: false,
          logout: true,

          login: {
            url: '/api/gamedata',
            method: 'post',
          },
        },
      },
    },
  },

  vuetify: {
    theme: {
      dark: false,
      themes: {
        light: {
          primary: colors.indigo.darken3,
          secondary: colors.cyan.darken2,
          accent: colors.lightGreen.lighten2,
          error: colors.red.base,
          warning: colors.amber.base,
          info: colors.lightBlue.base,
          success: colors.green.base,
        },
        dark: {
          primary: colors.indigo.darken3,
          secondary: colors.cyan.darken2,
          accent: colors.lightGreen.lighten2,
          error: colors.red.base,
          warning: colors.amber.base,
          info: colors.lightBlue.base,
          success: colors.green.base,
        },
      },
    },
  },
};

if (process.env.MATOMO_URL && process.env.MATOMO_SITEID) {
  nuxtConfig.modules.push(['nuxt-matomo', {
    matomoUrl: process.env.MATOMO_URL,
    siteId: process.env.MATOMO_SITEID,
  }]);
}

export default nuxtConfig;

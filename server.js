/**
 * @file The entry point of the whole application.
 * @author Daniel Steinhauer
 */

import express from 'express';
import morgan from 'morgan';

import createApiRouter from './api/router.js';
import createNuxt from './client/init-nuxt.js';

async function createExpressApp() {
  const app = express();

  const logger = morgan(morgan.dev);
  const apiRouter = createApiRouter();
  const nuxt = await createNuxt();

  app.use(logger);
  app.use('/api', apiRouter);
  app.use(nuxt.render);

  return app;
}

const port = process.env.PORT || 8080;

createExpressApp().then((app) => {
  app.listen(port, () => {
    process.stdout.write(`Node is listening on port ${port}...\n`);
  });
}).catch((error) => {
  process.stderr.write(error.stack);
});
